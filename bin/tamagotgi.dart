import 'dart:io';

abstract class tamagotgi {
  //hamster
  String name = "";
  int stamina = 100;
  int hp = 300;
  int atk = 10;
  int level = 1;

  tamagotgi(String name) {
    this.name = name;
    this.stamina = stamina;
    this.hp = hp;
    this.atk = atk;
    this.level = level;
  }

  int getStamina() {
    return this.stamina;
  }

  void setStamina(int stamina) {
    this.stamina = stamina;
  }

  int getHp() {
    return this.hp;
  }

  void setHp(int hp) {
    this.hp = hp;
  }

  int getAtk() {
    return this.atk;
  }

  void setAtk(int atk) {
    this.atk = atk;
  }

  int getLevel() {
    return this.level;
  }

  void setLevel(int level) {
    this.level = level;
  }
}
