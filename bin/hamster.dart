import 'dart:io';

import 'tamagotgi.dart';

class hamster extends tamagotgi {
  var hungry;
  var happiness;
  var cleanliness;

  int getHungry() {
    return this.hungry;
  }

  void setHungry(int hungry) {
    this.hungry = hungry;
  }

  int getHappiness() {
    return this.happiness;
  }

  void setHappiness(int happiness) {
    this.happiness = happiness;
  }

  int getCleanliness() {
    return this.cleanliness;
  }

  void setCleanliness(int cleanliness) {
    this.cleanliness = cleanliness;
  }

  hamster(super.name) {
    this.hungry = 30;
    this.happiness = 30;
    this.cleanliness = 30;
  }
//ค่าความหิว
  bool eat() {
    if (hungry <= 20) {
      hungry += 5;
      return true;
    }
    return false;
  }

//ค่าความสุข
  bool run() {
    if (happiness <= 20) {
      happiness += 5;
      return true;
    }
    return false;
  }

//ค่าความสะอาด
  bool shower() {
    if (cleanliness <= 20) {
      cleanliness += 5;
      return true;
    }
    return false;
  }

//เทรน
  bool training() {
    if (hungry >= 15 && happiness >= 15 && cleanliness >= 15) {
      hungry -= 7;
      happiness -= 11;
      cleanliness -= 12;
      stamina -= 10;
      hp -= 9;
      level += 2;
      atk += 10;
      return true;
    } else if (hungry < 10 || happiness < 10 || cleanliness < 10) {
      if (hungry < 10) {
        print("Hamter is hungry no train");
      } else if (happiness < 10) {
        print("Hamter is nothappiness no trian");
      } else if (cleanliness < 10) {
        print("Hamter notcleanliness no train");
      }
    }
    return false;
  }

//พักผ่อน
  bool rest() {
    if (stamina <= 20 && hp <= 20) {
      stamina += 20;
      hp += 20;
      return true;
    }
    return false;
  }

//skill
  void hamsjump() {
    atk += 40;
    stamina -= 30;
  }

  void hamsfight() {
    atk += 20;
    stamina -= 20;
  }

  bool hamsDead() {
    if (getHp() <= 0) {
      return true;
    }
    return false;
  }

  void showstatus() {
    int H = getHungry();
    print("Hungry : $H ");
    int Happy = getHappiness();
    print("Happiness : $Happy ");
    int Clean = getCleanliness();
    print("cleanliness : $Clean ");
    int hp = getHp();
    print("Hp : $hp ");
    int sta = getStamina();
    print("stamina : $sta ");
    int atk = getAtk();
    print("Atk : $atk ");
    int level = getLevel();
    print("Level : $level ");
  }
}
