import 'dart:io';

import 'hamster.dart';
import 'monster.dart';

void main() {
  int round = 15; //เล่น 15 รอบ แบ่งเป็น ดูแล10รอบ ต่อสู้ 5 รอบ

//ใส่ชื่อ
  print("Input Name Hamster : ");
  hamster hamtster = new hamster(stdin.readLineSync()!);
  monster monster1 = new monster("Dark Cat");

// เลือกเลี้ยงดูแล
  print("Please choose activities your Hamster");
  print("+++++++++++++++++++++++++++++++++++++++++");
  print(" 1.Eat  2.Run 3.Shower 4.Rest 5.Training ");
  print("+++++++++++++++++++++++++++++++++++++++++");

//ลูปการดูแลเลี้ยง
  while (round >= 5) {
    int activities = int.parse(stdin.readLineSync()!);
    switch (activities) {
      case 1: //การกิน
        if (hamtster.eat() == true) {
          round--;
          print("Number Round : $round");
          hamtster.showstatus();
          print("Choose Number");
        } else {
          print("Number Round : $round");
          hamtster.showstatus();
          print("Hamster is Full please choose new number");
        }
        break;
      case 2: //ความสนุก
        if (hamtster.run() == true) {
          round--;
          hamtster.showstatus();
          print("Choose Number");
        } else {
          print("Number Round : $round");
          hamtster.showstatus();
          print("Hamster is Happy please choose new number");
        }
        break;
      case 3: //ความสะอาด
        if (hamtster.shower() == true) {
          round--;
          print("Number Round : $round");
          hamtster.showstatus();
          print("Choose Number");
        } else {
          print("Number Round : $round");
          hamtster.showstatus();
          print("Hamster is Clean please choose new number");
        }
        break;
      case 4: //พักผ่อน
        if (hamtster.rest() == true) {
          round--;
          print("Number Round : $round");
          hamtster.showstatus();
          print("Choose Number");
        } else {
          print("Number Round : $round");
          hamtster.showstatus();
          print("Hamster is rest enough please choose new number");
        }
        break;
      case 5: //เทรน
        if (hamtster.training() == true) {
          round--;
          print("Number Round : $round");
          hamtster.showstatus();
          print("Choose Number");
        } else {
          print("Number Round : $round");
          hamtster.showstatus();
          print("Hamster is no training please choose new number");
        }
        break;
      default:
        print("Please choose activities your Hamster");
        print("1.Eat  2.Run 3.Shower 4.Rest 5.Training");
    }
  }
  while (round >= 0) {
    print("Your Hamster Found Monster name Dark Cat!!!");
    print("Please choose Skill for Atk");
    print(" 1. hamsfight  2. hamsjump ");
    int skill = int.parse(stdin.readLineSync()!);
    if (skill == 1) {
      hamtster.hamsfight();
      monster1.setHp(monster1.getHp() - hamtster.getAtk());
      int atk = hamtster.getAtk();
      int hp = hamtster.getHp();
      print("Atk Hamster : $atk ");
      print("Hp Hamster : $hp ");
      print("-------------------");
      int atkm = monster1.getAtk();
      int hpm = monster1.getHp();
      print("Atk Monster : $atkm ");
      print("Hp Monster : $hpm ");
      print("-------------------");
      if (monster1.Dead()) {
        print("Drak Cat Dead !!!! ");
        return;
      } else {
        hamtster.setHp(hamtster.getHp() - monster1.getAtk());
        if (hamtster.hamsDead()) {
          hamtster.hamsfight();
          monster1.setHp(monster1.getHp() - hamtster.getAtk());
          int atk = hamtster.getAtk();
          int hp = hamtster.getHp();
          print("Atk Hamster : $atk ");
          print("Hp Hamster : $hp ");
          print("-------------------");
          int atkm = monster1.getAtk();
          int hpm = monster1.getHp();
          print("Atk Monster : $atkm ");
          print("Hp Monster : $hpm ");
          print("-------------------");
          print("THE END !!!!!!!");
        }
      }
    } else {
      hamtster.hamsjump();
      monster1.setHp(monster1.getHp() - hamtster.getAtk());
      hamtster.hamsfight();
      monster1.setHp(monster1.getHp() - hamtster.getAtk());
      int atk = hamtster.getAtk();
      int hp = hamtster.getHp();
      print("Atk Hamster : $atk ");
      print("Hp Hamster : $hp ");
      print("-------------------");
      int atkm = monster1.getAtk();
      int hpm = monster1.getHp();
      print("Atk Monster : $atkm ");
      print("Hp Monster : $hpm ");
      print("-------------------");
      if (monster1.Dead()) {
        print("Drak Cat Dead !!!! ");
        return;
      } else {
        hamtster.setHp(hamtster.getHp() - monster1.getAtk());
        if (hamtster.hamsDead()) {
          hamtster.hamsfight();
          monster1.setHp(monster1.getHp() - hamtster.getAtk());
          int atk = hamtster.getAtk();
          int hp = hamtster.getHp();
          print("Atk Hamster : $atk ");
          print("Hp Hamster : $hp ");
          print("-------------------");
          int atkm = monster1.getAtk();
          int hpm = monster1.getHp();
          print("Atk Monster : $atkm ");
          print("Hp Monster : $hpm ");
          print("-------------------");
          print("THE END !!!!!!!");
        }
      }
    }
    round--;
    print("Number Round : $round");
  }
}
