import 'dart:io';

import 'tamagotgi.dart';

class monster extends tamagotgi {
  monster(super.name);
  String name = "Dark Cat";
  int hp = 200;
  int level = 9;
  int atk = 40;

  int getHp() {
    return this.hp;
  }

  void setHp(int hp) {
    this.hp = hp;
  }

  int getAtk() {
    return this.atk;
  }

  void setAtk(int atk) {
    this.atk = atk;
  }

  int getLevel() {
    return this.level;
  }

  void setLevel(int level) {
    this.level = level;
  }

  bool Dead() {
    if (getHp() <= 0) {
      return true;
    }
    return false;
  }
}
